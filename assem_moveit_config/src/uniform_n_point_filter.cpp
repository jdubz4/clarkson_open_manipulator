#include "assem_moveit_config/uniform_n_point_filter.h"
#include <moveit/planning_request_adapter/planning_request_adapter.h>
#include <class_loader/class_loader.h>

#include <ros/ros.h>
#include <ros/console.h>

using namespace assem_moveit_config;
         
UniformNPointFilter::UniformNPointFilter() : planning_request_adapter::PlanningRequestAdapter(), nh_("~") {
   if (!nh_.getParam(RATE_PARAMETER_NAME_, sample_time)) {
      ROS_INFO_STREAM("Param '" << RATE_PARAMETER_NAME_ << "' was not set. Using default rate.");
      sample_time = 0.25;
   }
   if (!nh_.getParam(COUNT_PARAMETER_NAME_, sample_count)) {
      ROS_INFO_STREAM("Param '" << COUNT_PARAMETER_NAME_ << "' was not set. Using default count.");
      sample_count = 10;
   }
}
         
UniformNPointFilter::~UniformNPointFilter() { };
         
std::string UniformNPointFilter::getDescription() const { return "Resamples trajectory to a specified sample count at a fixed sample rate."; }
         
bool UniformNPointFilter::adaptAndPlan(const PlannerFn& planner, const planning_scene::PlanningSceneConstPtr& planning_scene,
                          const planning_interface::MotionPlanRequest& req,
                          planning_interface::MotionPlanResponse& res,
                          std::vector<size_t> &added_path_index) const {
   bool result = planner(planning_scene, req, res);
   ROS_INFO_STREAM("UNIFORM_N_POINT_FILTER runs here!!!");
   return result;
}

const std::string UniformNPointFilter::RATE_PARAMETER_NAME_ = "/movegroup/uniform_n_point_filter_rate";
const std::string UniformNPointFilter::COUNT_PARAMETER_NAME_ = "/movegroup/uniform_n_point_filter_count";

CLASS_LOADER_REGISTER_CLASS(assem_moveit_config::UniformNPointFilter, planning_request_adapter::PlanningRequestAdapter)
