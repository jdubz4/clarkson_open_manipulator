#!/usr/bin/env python
import rospy
from sensor_msgs.msg import *
from std_msgs.msg import *

rospy.init_node("array_reader")
pub = rospy.Publisher("goal_dynamixel_position", JointState, queue_size = 1)

file_name = raw_input("File name: ")
hzs = raw_input("Clock hzs: ")
rate = rospy.Rate(int(hzs))

state = rospy.wait_for_message("joint_states", JointState)
state.header = Header();

data = []
with open(file_name) as file:
   for line in file:
      for num in line.split():
         data.append(float(num))
         

grip = state.position[6]
while data:
   point = []
   for i in range(0, 6):
      point.append(data.pop(0))
   point.append(grip)
   state.position = point
   pub.publish(state)
   rate.sleep()
