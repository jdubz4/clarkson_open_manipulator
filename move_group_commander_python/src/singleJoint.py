#!/usr/bin/env python
import math
import time
import rospy
from dynamixel_workbench_msgs.srv import *


rospy.wait_for_service("joint_command")

res = 20
rotate_joint_1 = rospy.ServiceProxy('joint_command', JointCommand)
for i in range(res + 1):
   angle = -math.pi + (((math.pi / 2) / res) * i)
   rotate_joint_1('rad', 1, angle)
   time.sleep(.5)
